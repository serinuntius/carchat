#coding:utf-8
from PyQt5.QtWidgets import (QApplication, QWidget,
                             QGridLayout, QVBoxLayout, QHBoxLayout,
                             QLabel, QLineEdit, QPushButton)
import os
import Record
import SpeechToText


"""
Speak Command
open_jtalk -x /usr/local/OpenJTalk/dic -m /usr/local/OpenJTalk/voice/m001/nitech_jp_atr503_m001.htsvoice -ow out.wav sample.txt
"""

r = Record.Record()
s = SpeechToText.SpeechToText()

class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.b = False
        self.Button = [QPushButton("右折"),QPushButton("左折"),QPushButton("駐車"),QPushButton("バック"),QPushButton("発進"),QPushButton("停車"),QPushButton("車線変更（右）"),QPushButton("車線変更（左）")]

        """
        self.RButton = QPushButton("右折")
        self.LButton = QPushButton("左折")
        self.RButton.clicked.connect(self.calc)
        self.LButton.clicked.connect(self.calc)
        """
        self.dic = {"right":60,"left":50,"parking":95,"back":90,"start":40,"stop":30,"changelaneR":55,"changelaneL":56}
        self.Button[0].clicked.connect(lambda:self.judge(self.dic["left"]))
        self.Button[1].clicked.connect(lambda:self.judge(self.dic["right"]))
        self.Button[2].clicked.connect(lambda:self.judge(self.dic["parking"]))
        self.Button[3].clicked.connect(lambda:self.judge(self.dic["back"]))
        self.Button[4].clicked.connect(lambda:self.judge(self.dic["start"]))
        self.Button[5].clicked.connect(lambda:self.judge(self.dic["stop"]))
        self.Button[6].clicked.connect(lambda:self.judge(self.dic["changelaneR"]))
        self.Button[7].clicked.connect(lambda:self.judge(self.dic["changelaneL"]))

        self.Label = QLabel(self)
        self.Label2 = QLabel(self)

        #labelLayout = QVBoxLayout()
        #labelLayout.addWidget(self.Label)
        self.Label.setText("〇〇中")
        self.Label2.setText("残りリソース◯◯%")



        buttonLayout = QVBoxLayout()
        buttonLayout.addWidget(self.Label)

        for i in self.Button[:4]:
            buttonLayout.addWidget(i)
        buttonLayout2 = QVBoxLayout()
        buttonLayout2.addWidget(self.Label2)
        for i in self.Button[4:]:
            buttonLayout2.addWidget(i)

        mainLayout = QHBoxLayout()
        mainLayout.addLayout(buttonLayout)
        mainLayout.addLayout(buttonLayout2)
        #mainLayout.addLayout(labelLayout)


        self.setLayout(mainLayout)
        self.setWindowTitle("運転状況シミュレーター")

    def judge(self,num):
        self.Label2.setText("残りリソース"+str(100-num)+"%")
        speak_file = open("speak.txt","w")

        if num == 50:
            self.Label.setText("右折中")
            #r.record()
            if os.path.isfile("voice.wav"):
                text = s.stt_google_wav("voice.wav")
                if text:
                    speak_file.write(text.encode('utf_8'))
                    speak_file.close()#書いたら閉じる
                    os.system("open_jtalk -x /usr/local/OpenJTalk/dic -m /usr/local/OpenJTalk/voice/m001/nitech_jp_atr503_m001.htsvoice -ow out.wav speak.txt")
        elif num == 60:
            self.Label.setText("左折中")
        elif num == 95:
            self.Label.setText("駐車中")
        elif num == 90:
            self.Label.setText("バック中")
        elif num == 40:
            self.Label.setText("スタート")
        elif num == 30:
            self.Label.setText("ストップ")
        elif num == 55:
            self.Label.setText("車線変更（右）")
        elif num == 56:
            self.Label.setText("車線変更（左）")


        print "残りリソース" + str(100-num) + "％"



if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    main_window = MainWindow()

    main_window.show()
    sys.exit(app.exec_())
