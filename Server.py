#coding:utf-8
import socket
import threading
from buffer import set_list


class Server:
    def __init__(self,address = "",port = 50007):
        #self.HOST = 'localhost'
        #サーバプログラムを動作させるホストを入力
        self.HOST = address
        self.PORT = port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind((self.HOST, self.PORT))#サーバのアドレスをソケットに設定


    def listen(self):
        self.s.listen(1) #１つの接続要求を待つ
        print "waiting client"
        self.soc, self.addr = self.s.accept() #要求が来るまでブロックする
    def recv(self):
        self.msg =  self.soc.recv(1024)
        print self.msg
        set_list(self.msg)
        self.th = threading.Thread(target=self.recv)
        self.th.start()

    def send(self,message):
        message = message.encode("utf-8")
        self.soc.send(message)
    def close(self):
        self.soc.close()
