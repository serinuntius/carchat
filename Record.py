#coding:utf-8
import pyaudio
import sys
import time
import wave

class Record:
    """To record voice.wav"""
    def __init__(self):
        self.chunk = 1024
        self.FORMAT = pyaudio.paInt16
        self.CHANNELS = 1
        #サンプリングレート、マイク性能に依存
        self.RATE = 44100
        #録音時間
        self.RECORD_SECONDS =  3
        #input('Please input recoding time>>>')

    def record(self):
        #pyaudio
        p = pyaudio.PyAudio()
        #マイク0番を設定
        input_device_index = 0
        #マイクからデータ取得
        stream = p.open(format = self.FORMAT,
                        channels = self.CHANNELS,
                        rate = self.RATE,
                        input = True,
                        frames_per_buffer = self.chunk)
        all = []
        print "record now!"
        for i in range(0, self.RATE / self.chunk * self.RECORD_SECONDS):
            data = stream.read(self.chunk)
            all.append(data)
        stream.close()
        data = ''.join(all)
        out = wave.open('voice.wav','w')
        out.setnchannels(1) #mono
        out.setsampwidth(2) #16bits
        out.setframerate(self.RATE)
        out.writeframes(data)
        out.close()
        p.terminate()
        print "record end!"
