#coding:utf-8
import os
import Record
from SpeechToText import *
import sys
from Tkinter import *
import Client
import Server
import threading
from tkMessageBox import *
from buffer import get_list
from speak_lib import *


text = ["右折","左折","バック","発進","停車","直進(街中)","直進(高速)","車線変更（右折）","車線変更（左折）"]
res = [70,65,85,60,10,50,40,85,80]
listen_res = 20
speak_res = 40
watch_res = 60
write_res = 80

class Window:

    def __init__(self):
        self.root = Tk()
        self.root.option_add('*font',('FixedSys',14))
        self.root.title(u"CarChat")
        self.root.minsize(width=320, height=250)

        self.f = LabelFrame(self.root,text = "シミュレーション")

        self.menubar = Menu(self.root)
        self.root.configure(menu = self.menubar)
        self.command = Menu(self.menubar,tearoff = False)
        self.menubar.add_cascade(label = "Menu",underline = 0,menu = self.command)
        self.command.add_command(label = "Server",under = 0,command = self.wait_for_client)
        self.command.add_command(label = "Client",under = 0,command = self.connect_to_server)
        #ボタンを生成
        Button(self.f,text = text[0],command = lambda:self.judge(0)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[1],command = lambda:self.judge(1)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[2],command = lambda:self.judge(2)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[3],command = lambda:self.judge(3)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[4],command = lambda:self.judge(4)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[5],command = lambda:self.judge(5)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[6],command = lambda:self.judge(6)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[7],command = lambda:self.judge(7)).pack(in_=self.f,fill = BOTH)
        Button(self.f,text = text[8],command = lambda:self.judge(8)).pack(in_=self.f,fill = BOTH)


        self.f.pack(fill = BOTH,side=LEFT)

        #シミュレーションの右側にGUI提示用ラベル設置
        self.buf = StringVar()
        self.buf.set("")
        self.f2 = LabelFrame(self.root,text="メッセージ")

        self.label_text = StringVar()
        self.label_text.set("メッセージ待ちですllllllllll")
        self.label = Label(self.f2,textvariable = self.label_text)

        self.label.pack()
        self.e = Entry(self.f2,textvariable = self.buf,state = "disabled")
        self.e.bind('<Return>',self.send_keyboard_msg)
        self.e.pack()


        self.f2.pack(fill = BOTH,side = RIGHT)


        self.SorC = "" #鯖かクライアントかを保持する変数



    def send_keyboard_msg(self): #キーボード入力の送信用
        msg = self.buf.get()
        if msg:
            self.buf.set("")
            if self.SorC == "S":
                self.s.send(msg)
            elif self.SorC == "C":
                self.c.send(msg)

    def send(self,msg = None):
        if msg:
            if self.SorC == "S":
                self.s.send(msg)
            elif self.SorC == "C":
                self.c.send(msg)

    def recognize(self): #呼び出したら録音して、音声認識した結果を返す
        r = Record.Record()

        r.record()
        speak_file = open("speak.txt","w")
        if os.path.isfile("voice.wav"):
            text = stt_google_wav("voice.wav")
            if text:
                print text
                return text
            else: #音声認識失敗
                return None
        else:
            raise Exception("voice.wav does not found!")


    def judge(self,x):
        if self.SorC == "":
            showerror(title="Error!",message=u"メニュバーからサーバかクライアントか選択してください")
        else:
            rest = 100 -res[x]
            if rest >= listen_res:
                #listen
                print "listen"
                partner_speak = get_list()
                if partner_speak:
                    for i in partner_speak:
                        speak(i)
                #rest -= listen_res


            if rest >= speak_res:
                #speak
                print "speak"
                speech = self.recognize() #return str
                if speech: #音声認識結果があれば
                    self.send(msg = speech)
                #rest -= speak_res

            if rest >= watch_res:
                #watch
                print "watch"
                #rest -= watch_res


            if  rest >= write_res:
                #書く
                self.e.configure(state = "normal")
                self.e.focus_set()
                print "write"
                #rest -= write_res
            print "rest res:" + str(rest)
            print



    def connect_to_server(self):
        self.c = Client.Client()
        self.thc = threading.Thread(target=self.c.recv)
        self.thc.start()
        self.SorC = "C"

        print "connect_to_server"
    def wait_for_client(self):
        self.s = Server.Server()
        print "wait_for_client"
        self.s.listen()
        print "connect!"
        self.ths = threading.Thread(target=self.s.recv)
        self.ths.start()
        self.SorC = "S"
        """
        if self.s.msg:
            print self.s.msg
        """
        print "wait_for_client"
if __name__ == '__main__':
    window = Window()
    window.root.mainloop()
