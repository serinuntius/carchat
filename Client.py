#coding:utf-8
import socket
import threading
from buffer import set_list

class Client:
    def __init__(self,address = "localhost",port = 50007):
        self.HOST = address
        #サーバプログラムを動作させるホストを入力
        self.PORT = port

        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.HOST, self.PORT))
    def recv(self):
        self.msg = self.s.recv(1024)
        print self.msg
        set_list(self.msg)
        self.th = threading.Thread(target=self.recv)
        self.th.start()

    def send(self,message):
        message = message.encode("utf-8")
        self.s.send(message)
    def close(self):
        self.s.close()
